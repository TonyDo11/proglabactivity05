package polymorphism.inheritance;

public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String title, String author, int numberBytes) {
       super(title, author); 
        this.numberBytes = numberBytes;
    }

    public int getNumberBytes() {
        return numberBytes;
    }

    @Override
    public String toString() {
       String bookInfo = super.toString();
       return bookInfo + " Bytes: " + numberBytes;
    }
}
