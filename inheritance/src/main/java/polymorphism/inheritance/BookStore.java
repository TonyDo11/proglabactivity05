package polymorphism.inheritance;

public class BookStore {
    public static void main(String[]args) {

        Book[] books = new Book[5];
        books[0] = new Book("Book 1", "Author 1");
        books[1] = new ElectronicBook("E-Book 1", "Author 2", 1024);
        books[2] = new Book("Book 2", "Author 3");
        books[3] = new ElectronicBook("E-Book 2", "Author 4", 512);
        books[4] = new ElectronicBook("E-Book 3", "Author 5",  768);

        for (Book book: books) {
            System.out.println(book);
        }
    }
}
